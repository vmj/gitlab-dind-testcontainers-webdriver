import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.nio.file.Path;

import static org.testcontainers.containers.BrowserWebDriverContainer.VncRecordingMode.RECORD_FAILING;
import static org.testcontainers.containers.BrowserWebDriverContainer.VncRecordingMode.SKIP;
import static org.testcontainers.images.PullPolicy.defaultPolicy;

@Testcontainers
public class TestBase {
    protected final Logger log = LoggerFactory.getLogger(TestBase.class);

    @Container
    private BrowserWebDriverContainer<?> browser = new BrowserWebDriverContainer<>()
            .withCapabilities(new FirefoxOptions())
            .withImagePullPolicy(defaultPolicy())
            .withRecordingMode(
                    // Screen recording doesn't seem to work on GitLab
                    System.getenv("CI") != null ? SKIP : RECORD_FAILING,
                    Path.of("build", "test-results", "videos").toFile()
            )
            .withRecordingFileFactory(new Recordings())
            ;
    protected WebDriver driver;

    @BeforeEach
    protected void setUpTests() {
        driver = browser.getWebDriver();
    }

}

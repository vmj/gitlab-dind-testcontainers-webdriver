import org.junit.jupiter.api.Test;

import java.time.Duration;

public class FooTest extends TestBase {

    @Test
    void performTest() throws InterruptedException {
        for (int i = 0; i < 24; i++) {
            log.info("{}: Loading page...", i);
            driver.get("https://front.dev-my.docrates.com");
            log.info("{}: This is fine.", i);
            log.info("{}: Sleeping to mimic a longer test...", i);
            Thread.sleep(Duration.ofSeconds(5).toMillis());
            log.info("{}: Yawn", i);
        }

    }

}

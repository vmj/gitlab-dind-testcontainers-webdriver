import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.RecordingFileFactory;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.toMap;

public class Recordings implements RecordingFileFactory {
    private static final Logger log = LoggerFactory.getLogger(Recordings.class);

    private static final DateTimeFormatter TIMESTAMP_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-nnn");
    private static final Pattern DESCRIPTION_PATTERN = Pattern.compile("%5B(\\w+)%3A([\\w-]+)(?:%28%29)?%5D(?:%2F|_)?");
    private static final String SUCCESS_SUFFIX = "success";
    private static final String FAILURE_SUFFIX = "failure";

    @Override
    public File recordingFileForTest(File directory, String description, boolean succeeded) {
        // description: %5Bengine%3Ajunit-jupiter%5D%2F%5Bclass%3AFooTest%5D%2F%5Bmethod%3AperformTest%28%29%5D_
        // URL decoded: [engine:junit-jupiter]/[class:FooTest]/[method:performTest()]_
        //
        // But we don't bother URL decoding it; our pattern is using the percent
        // encoded representations.
        var test = DESCRIPTION_PATTERN.matcher(description)
                .results()
                .collect(toMap(
                        (MatchResult r) -> r.group(1),
                        (MatchResult r) -> r.group(2)));
        var className = test.get("class");
        var methodName = test.get("method");
        if (className == null || methodName == null)
            throw new RuntimeException("Unable to parse class and method names from test description: '" + description + "'");

        var timestamp = ZonedDateTime.now().format(TIMESTAMP_FORMAT);

        var suffix = succeeded ? SUCCESS_SUFFIX : FAILURE_SUFFIX;

        var filename = String.format("%s_%s_%s_%s.flv", timestamp, className, methodName, suffix);

        // Apparently it is our responsibility to create the file path.
        // FIXME: since we have a flat structure (all recordings in a single directory),
        //        move this to a place where it is executed only once; maybe build.gradle.
        if (directory.mkdirs())
            log.info("Created screen recording directory: {}", directory.getAbsolutePath());

        return directory.toPath().resolve(filename).toFile();
    }
}
